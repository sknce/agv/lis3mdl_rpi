#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/spi.h"
#include "../inc/config.hpp"
#include <cstdint>
#include <iostream>

int main()
{
    // Pins
    const uint cs_pin = 22;
    const uint sck_pin = 24;
    const uint mosi_pin = 25;
    const uint miso_pin = 21;

    // Ports
    spi_inst_t *spi = spi0;

    // Initialize chosen serial port
    stdio_init_all();
    // Initialize CS pin high
    gpio_init(cs_pin);
    gpio_set_dir(cs_pin, GPIO_OUT);
    gpio_put(cs_pin, 1);

    // Initialize SPI port at 1 MHz
    spi_init(spi, 1000 * 1000);

    // Set SPI format
    spi_set_format(spi0,       // SPI instance
                   8,          // Number of bits per transfer
                   SPI_CPOL_1, // Polarity (CPOL)
                   SPI_CPHA_1, // Phase (CPHA)
                   SPI_MSB_FIRST);

    // Initialize SPI pins
    gpio_set_function(sck_pin, GPIO_FUNC_SPI);
    gpio_set_function(mosi_pin, GPIO_FUNC_SPI);
    gpio_set_function(miso_pin, GPIO_FUNC_SPI);

    sleep_ms(2000);
    uint8_t msg = 0x2e;
    uint8_t data = 0x00;
    // Loop forever
    while (true)
    {
        // Construct message (set ~W bit high)

        // Read from register
        gpio_put(cs_pin, 0);
        spi_write_blocking(spi, &msg, 1);
        spi_read_blocking(spi, 0, &data, 1);
        gpio_put(cs_pin, 1);
        sleep_ms(100);
        std::cout << data;
    }

    return 0;
}