#ifndef CONFIG_HPP
#define CONFIG_HPP

// LIS3MDL Mag Registers

#define LIS3MDL_M_ADDRESS 0x1E      //setting_device adress
#define LIS3MDL_WHO_AM_I_M 0x0F     //id
#define LIS3MDL_CTRL_REG1_M 0x20
#define LIS3MDL_CTRL_REG2_M 0x21
#define LIS3MDL_CTRL_REG3_M 0x22
#define LIS3MDL_CTRL_REG4_M 0x23
#define LIS3MDL_STATUS_REG_M 0x27
#define LIS3MDL_OUT_X_L_M 0x28
#define LIS3MDL_OUT_X_H_M 0x29
#define LIS3MDL_OUT_Y_L_M 0x2A
#define LIS3MDL_OUT_Y_H_M 0x2B
#define LIS3MDL_OUT_Z_L_M 0x2C
#define LIS3MDL_OUT_Z_H_M 0x2D
#define LIS3MDL_TEMP_OUT_L_M 0x2E
#define LIS3MDL_TEMP_OUT_H_M 0x2F
#define LIS3MDL_INT_CFG_M 0x30
#define LIS3MDL_INT_SRC_M 0x31
#define LIS3MDL_INT_THS_L_M 0x32
#define LIS3MDL_INT_THS_H_M 0x33

// reset and tempreture select
#define LIS3MDL_REG_CTL_1_TEMP_EN 0x80
#define LIS3MDL_REG_CTL_2_RESET 0x04

// mag_scale defines all possible FSR's of the magnetometer:

#define LIS3MDL_M_SCALE_4GS 0x20  // 00:  4Gs
#define LIS3MDL_M_SCALE_8GS 0x40  // 01:  8Gs
#define LIS3MDL_M_SCALE_12GS 0x60 // 10:  12Gs
#define LIS3MDL_M_SCALE_16GS 0x60 // 11:  16Gs

// mag_oder defines all possible output data rates of the magnetometer:

#define LIS3MDL_M_ODR_625 0x04  // 6.25 Hz
#define LDIS3MDL_M_ODR_125 0x08 // 12.5 Hz
#define LIS3MDL_M_ODR_25 0x0C   // 25 Hz
#define LIS3MDL_M_ODR_5 0x10    // 50 Hz
#define LIS3MDL_M_ODR_10 0x14   // 10 Hz
#define LIS3MDL_M_ODR_20 0x14   // 20 Hz
#define LIS3MDL_M_ODR_40 0x14   // 40 Hz
#define LIS3MDL_M_ODR_80 0x14   // 80 Hz

#endif // CONFIG_HPP